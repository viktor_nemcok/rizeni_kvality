*** Settings ***
Library    Selenium2Library
Resource   ../Settings/Imports.txt 
Test Setup    Open Google    ${url}    ${browser}
Test Teardown    Capture Screenshot And Close Browser

*** Variables ***
${search_word}    VSE
${search_site}    www.vse.cz
${search_text_heading}    Vysoká škola ekonomická

*** Test Cases ***
Google Search Engine
    Search In Google Form    ${input_search}    ${search_word}   
    Page Waits For Element And Checks It    ${first_search_site}    ${search_site}
    Click Element    ${first_search_site}
    Page Waits For Element And Checks It    ${search_tag_heading}    ${search_text_heading}    